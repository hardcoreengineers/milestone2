﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using PL_GUI;
using BL;
using DAL;

namespace GUILauncher
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            IDAL theDAL = new DataAccess();
            IBL theBL = new UserManagment(theDAL);
            IPL thePL = new Program(theBL);
            thePL.Run();
        }
    }
}
