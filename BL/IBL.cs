﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedClasses;

namespace BL
{
    public interface IBL
    {
        bool verify(string username, string password);
        bool changePassword(User user,string pass);
        bool checkPass(string s);
        string getRandomPassword();
    }
}
