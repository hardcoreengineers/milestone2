﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SharedClasses;

namespace DAL
{
    public class DataAccess : IDAL
    {
        readonly static string address = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)+"/Database.txt";
        public DataAccess()
        {

        }
        public bool verify(string username, string password)
        {
            bool answer = false;
            bool found = false;
            var users = File.ReadAllLines(address);
            for (int i = 0; i < users.Length && !found; i++)
            {
                if (users[i] == username)
                {
                    found = true;
                    if (users[i + 1] == password)
                    {
                        answer = true;
                    }
                }
            }
            return answer;
        }
        public String[] getData()
        {
            return File.ReadAllLines(address);
        }
        public void changeData(string[] users)
        {
            File.WriteAllLines(address, users);
        }
    }
}
