﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using SharedClasses;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for Account.xaml
    /// </summary>
    public partial class Account : UserControl
    {
        private User me;
        private IBL myBL;

        public Account(IBL myBL, User me)
        {
            this.myBL = myBL;
            this.me = me;
            InitializeComponent();
        }

        private void ChangePassword_Click(object sender, RoutedEventArgs e)
        {
            UserControl newPassword = new NewPassword(myBL,me);
            this.Content = newPassword;
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            MainMenu MM = new MainMenu(myBL,me);
            this.Content = MM;
        }
    }
}
