﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using SharedClasses;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : UserControl
    {
        IBL myBL;
        User me;
        public MainMenu(IBL theBL, User user)
        {
            myBL = theBL;
            me = user;
            InitializeComponent();
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Goodbye! , " + me.getUsername());

            LoginMenu lm = new LoginMenu(myBL);
            this.Content = lm;

        }


        private void Account_Button_Click(object sender, RoutedEventArgs e)
        {
            UserControl uc = new Account(myBL, me);
            this.Content = uc;
            
        }

        private void ET_button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DLT_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void UM_Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
