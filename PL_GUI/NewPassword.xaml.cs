﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using SharedClasses;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for NewPassword.xaml
    /// </summary>
    public partial class NewPassword : UserControl
    {
        private User me;
        private IBL myBL;
        public NewPassword(IBL myBL, User me)
        {
            this.myBL = myBL;
            this.me = me;
            InitializeComponent();
        }

        private void ChangePassword_Click(object sender, RoutedEventArgs e)
        {
            String password = PasswordTextBox.Password;
            bool s = myBL.changePassword(me, password);
            if (myBL.changePassword(me, password))
            {
                MessageBox.Show("Password has changed successfully! ");
            }
            else
            {
                MessageBox.Show("Password must contain 8 characters and and at least one letter.");
            }
        }

        private void RandomPasswordButton_Click(object sender, RoutedEventArgs e)
        {
            String password = myBL.getRandomPassword();
            myBL.changePassword(me,password);
            NewPasswordText.Text = password;
            this.RandomPasswordStatus.Visibility = Visibility.Visible;
            this.NewPasswordText.Visibility = Visibility.Visible;
        }

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            if(Key.Enter == e.Key)
            {
                Button_Click(sender, e);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Account ac = new Account(myBL, me);
            this.Content = ac;
        }
    }
}
